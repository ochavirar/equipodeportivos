import { Component, OnInit } from '@angular/core';
import { Equipos } from '../../models/equipos';
import { EquiposService } from '../../services/equipos.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { Jugadores } from 'src/app/models/jugadores';
import { JugadoresService } from '../../services/jugadores.service';


@Component({
  selector: 'app-data-details',
  templateUrl: './data-details.page.html',
  styleUrls: ['./data-details.page.scss'],
})
export class DataDetailsPage implements OnInit {
  equiposs:Equipos={
    ciudad:"",
    foto:"",
    nombre:""
  }

  jugadoress:Jugadores={
    nombre:"",
    foto:"",
    equipoId:""
  }
  jugadores: Jugadores[];
  equipoId = null;
  constructor(private route: ActivatedRoute, private nav: NavController,
    private dataService: EquiposService, private loadingController: LoadingController,
    private jugadoresService:JugadoresService) { }

  ngOnInit() {
    this.equipoId = this.route.snapshot.params['id'];
    this.jugadoresService.getPorEquipo(this.equipoId).subscribe(res => {
      console.log('Jugadores', res);
      this.jugadores = res;
    });
  }
  
}
