import { Component, OnInit } from '@angular/core';
import { Equipos } from '../models/equipos';
import { EquiposService } from '../services/equipos.service';
import { JugadoresService } from '../services/jugadores.service';
import { Jugadores } from '../models/jugadores';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  equipos: Equipos[];
  
  constructor(private equiposService: EquiposService) {}
  ngOnInit() {
    this.equiposService.getAllEquipos().subscribe(res => {
      console.log('Equipos', res);
      this.equipos = res;
    });
    
  }


}
