import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Jugadores} from '../models/jugadores';

@Injectable({
  providedIn: 'root'
})
export class JugadoresService {
  db = null;
  constructor(db: AngularFirestore) {
    this.db = db;
  }
  
  getPorEquipo(equipoId:string){
    var x = this.db.collection('jugadores', ref => ref.where('equipoId', '==', equipoId));
    var jugadores = x.snapshotChanges().pipe(map(
      actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data}
        });
      }
    ))
    return jugadores;
  }
}