import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Equipos} from '../models/equipos';

@Injectable({
  providedIn: 'root'
})
export class EquiposService {
  private equiposCollection : AngularFirestoreCollection<Equipos>;
  private equipos : Observable<Equipos[]>;

  constructor(db: AngularFirestore) {
    this.equiposCollection = db.collection<Equipos>('equipos');
    this.equipos = this.equiposCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map( a => {
          const equipos = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...equipos}
        });
      }
    ))
  }
  getAllEquipos() {
    return this.equipos;
  }
  getEquipos(id:string) {
    return this.equiposCollection.doc<Equipos>(id).valueChanges();
  }  
}
